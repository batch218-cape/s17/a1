/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    let printInfo = function printUser(){
        alert("Hi! Please add the info of your user.");
        let fullName = prompt("Enter your user's full name:"); 
        let age = prompt("Enter your user's age:"); 
        let location = prompt("Enter your user's location:");

        console.log("Hello, " + fullName); 
        console.log("You are " + age + " years old"); 
        console.log("You live in " + location); 
    };

    printInfo();
    

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

let printFavoriteBands = function printBands() {
    
    let band1 = "The Beatles";
    console.log("1. " + band1);

    let band2 = "Metallica";
    console.log("2. " + band2);

    let band3 = "The Eagles";
    console.log("3. " + band3);

    let band4 = "Taylor Swift";
    console.log("4. " + band4);

    let band5 = "Eraserheads";
    console.log("5. " + band5);
}

printFavoriteBands();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    let printFavoriteMovies = function printMovies() {
    
        let movie1 = "The Godfather";
        let movieRating1 = 97;
        console.log("1. " + movie1);
        console.log("Rotten Tomatoes Rating: " + movieRating1 + "%");

        let movie2 = "The Godfather, Part II";
        let movieRating2 = 96;
        console.log("2. " + movie2);
        console.log("Rotten Tomatoes Rating: " + movieRating2 + "%");

        let movie3 = "Shawshank Redemption";
        let movieRating3 = 91;
        console.log("3. " + movie3);
        console.log("Rotten Tomatoes Rating: " + movieRating3 + "%");

        let movie4 = "To Kill A Mockingbird";
        let movieRating4 = 93;
        console.log("4. " + movie4);
        console.log("Rotten Tomatoes Rating: " + movieRating4 + "%");

        let movie5 = "Psycho";
        let movieRating5 = 96;
        console.log("5. " + movie5);
        console.log("Rotten Tomatoes Rating: " + movieRating5 + "%");
    
    }

    printFavoriteMovies();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);

printFriends();
